from django.shortcuts import render
from cmdb import models

# 项目参考http://www.cnblogs.com/Leo_wl/p/5824541.html

# Create your views here.

def index(request):
    if request.method == "POST":
        account = request.POST.get("tboxAccount", None)
        password = request.POST.get("tboxPassword", None)
        # 添加数据到数据库
        models.UserInfo.objects.create(account=account, pwd=password)
    # 从数据库中读取所有数据
    user_list = models.UserInfo.objects.all()

    return render(request, "index.html", {"data": user_list})